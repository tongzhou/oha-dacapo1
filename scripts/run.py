import subprocess
import os
import sys
import shutil
import time
import datetime

import gg
import asm

def runj9AndHotSpot():
    #benches = ['tradebeans', 'tradesoap', 'jython', 'sunflow', 'h2']
    #benches = ['tradebeans', 'tradesoap', 'sunflow', 'h2']
    benches = ['avrora', 'fop', 'eclipse', 'fop', 'lusearch', 'luindex', 'xalan', 'pmd']
    runRaw(benches, 20, gg.j9)
    runRaw(benches, 20, gg.hotspot)
        

def runj9(benches=None, n=1, cfg='vanilla', jopts="", jit="", env=None,
          useEA=False, callback='OHAHelper', rmlogs=True):
    if rmlogs:
        os.system('rm -rf log.*')

    logname = 'log'
    if useEA:
        logname = 'ea+trans+log'
    if not jit:
        jit = 'log=%s,disableAsyncCompilation' % logname
    else:
        jit = 'log=%s,disableAsyncCompilation' % logname + jit

    os.system('bash ../copy_openj9_binaries.sh')    
    
    run(benches, n, cfg, jopts, jit, env, useEA, callback, gg.j9)


def runRaw(benches=None, n=1, java=gg.hotspot):
    for bench in getBenches(benches):
        if java == gg.hotspot:            
            timefile = bench+'_hs.time'
        else:
            timefile = bench+'_j9.time'
        cmd = ['time', '-o', timefile, '-v', java, '-cp', '..:../harness:']
        if bench.startswith('trade') and java == gg.j9:
            cmd += ['-XX:+PositiveIdentityHash']

        cmd += ['Harness', '-v', bench, '-n', str(n)]
        print(cmd)
        ret = subprocess.run(cmd)
        code = ret.returncode 
        if code != 0:
            print("return code:", code)
            print("failed cmd (cmd cannot contain empty str):", ' '.join(cmd))


def runVanilla(benches=None, n=1, jopts="", jit="", env=None, usePoa=False, useEA=True, callback=''):
    pass


def run(benches=None, n=1, cfg='vanilla', jopts="", jit="", env=None, useEA=False, callback='OHAHelper', java=gg.hotspot):
    '''
    Run a config.

    Examples:
    > # Run lusearch and print out final counter after each iteration.
    > run('lusearch', cfg='counter', n=10, callback='OHAHelper')  
    '''
    compileOHAHelper(callback)
        
    for bench in getBenches(benches): 
        #makeCnf(bench, cfg)
        cmd = [java, '-cp', '..:../harness:../ea-jars/plume-util-1.0.4.jar']
        if jopts:
            cmd += [jopts]
        if jit:
            cmd += ['-Xjit:%s' % jit]
        cmd += ['Harness']
        if callback:
            cmd += ['-c', callback]
        cmd += ['-v', bench, '-n', str(n), '-t', '4', '--preserve']
        #cmd += ['-v', bench, '-n', str(n), '--preserve']    
       
        print(' '.join(cmd))
        
        if gg.fake:
            continue

        env = {}
        if useEA:
            env = {
                "UseEA": "1",
                "OHA_interThresh": '30',
                "OHA_interBBThresh": '40',
                "OHA_BDDDir": '../bdds/%s' % bench,
                "OHA_cng": gg.getBenchDir(bench) + "cng.txt",
                "OHA_metadata": gg.getBenchDir(bench) + "metadata.txt"
                }
        env["OHA_benchDir"] = gg.getBenchDir(bench)

        print(env) 
        if env is None:
            ret = subprocess.run(cmd)
        else:
            ret = subprocess.run(cmd, env=env)
        code = ret.returncode 
        
        if code != 0:
            print("return code:", code)
            print("failed cmd:", ' '.join(cmd))

        if os.path.isfile('blockCounts.txt'):
            os.rename('blockCounts.txt', '../bms/%s/blockCounts.txt' % bench)


def runtest(benches='100', n=1, cfg='vanilla', jopts="", jit="", env=None, useEA=False,
            callback='OHAHelper', java=gg.hotspot, rmlogs=1):
    '''
    Run a config.

    Examples:
    > # Run lusearch and print out final counter after each iteration.
    > run('100', cfg='counter', callback='OHAHelper')  
    '''
    compileOHAHelper(callback)
    if useEA:
        jit += ',log=log,disableAsyncCompilation'
        os.system('bash ../copy_openj9_binaries.sh')

    if rmlogs:
        os.system('rm -rf log.*')
    for bid in benches.split():
        bench = 'TestPrograms.Test' + bid
        classdir = bench + '-' + cfg
        cmd = [java, '-cp', '../oha-jar/%s:../harness:../ea-jars/plume-util-1.0.4.jar' % classdir]
        if jopts:
            cmd += [jopts]
        if jit:
            cmd += ['-Xjit:%s' % jit]
        cmd += [bench]
       
        print(' '.join(cmd))
        
        if gg.dryrun:
            continue

        env = {}
        if useEA:
            env = {
                "UseEA": "1",
                "OHA_interThresh": '30',
                "OHA_interBBThresh": '40',
                "OHA_BDDDir": '../bdds/%s' % bench,
                "OHA_cng": gg.getBenchDir(bench) + "cng.txt",
                "OHA_metadata": gg.getBenchDir(bench) + "metadata.txt"
                }
        env["OHA_benchDir"] = gg.getBenchDir(bench)

        print(env) 
        if env is None:
            ret = subprocess.run(cmd)
        else:
            ret = subprocess.run(cmd, env=env)
        code = ret.returncode 

        if code != 0:
            print("return code:", code)
            print("failed cmd:", ' '.join(cmd))

        if os.path.isfile('blockCounts.txt'):
            os.rename('blockCounts.txt', '../bms/%s/blockCounts.txt' % bench)


def runAll(n=1, ils=0):
    gg.setAllBenches()
    run(n, ils)


def runFromScratch(bench):
    runPoa(bench, n=10)
    build([bench], ils=['vanilla', 'counter', 'counter+intraEA', 'counter+interEA'], opts=['-round', '3'])
    #build([bench], ils=['counter+interEA'], opts=['-round', '3'])
    run([bench], cfg='counter', n=10, useEA=False, callback='OHAHelper')
    run([bench], cfg='counter+intraEA', n=10, useEA=False, callback='OHAHelper')
    run([bench], cfg='counter+interEA', n=10, useEA=False, callback='OHAHelper')
    
def buildAndRunj9(bench):
    build([bench], ils=['counter+interEA+OHA'], opts=['-round', '3'])
    runj9([bench], cfg='counter+intraEA+OHA', n=10, useEA=True, callback='OHAHelper')
    
 
def run1(benches=None):
    # Get reflection profiles
    runPoa(benches, n=1)

    # Just instrument
    build(ils=[2], opts=['-io'])

    # Get block counts
    run(ils=2, n=3, useEA=False,callback='OHAHelperOffline')

    # Run EA again
    build(ils=[2], opts=['-io', 'false'])
    

def getBenches(benches):
    if not benches:
        benches = gg.bms

    if benches == 'all':
        benches = ['avrora', 'eclipse', 'fop', 'h2', 'luindex', 'lusearch', 'pmd', 'sunflow', 'xalan']
        
    if type(benches) == str:
        benches = [benches]

    return benches


def runPoa(benches=None, n=1, jopts="", jit="", env=None):
    gg.java = gg.hotspot
    if os.path.isdir('out'):
        shutil.rmtree('out')
        
    for bench in getBenches(benches):
        makeCnf(bench, '')
        cmd = [gg.java, '-cp', '..:../harness']            
        cmd += ['-javaagent:' + '../'+gg.poaJar]
        if jopts:
            cmd += [jopts]
        if jit:
            cmd += ['-Xjit:%s' % jit]
        #cmd += ['Harness', '-c', 'OHAHelper']
        cmd += ['Harness']
        cmd += ['-v', bench, '-n', str(n)]
        print(' '.join(cmd))
        
        if gg.fake:
            continue
        
        ret = subprocess.run(cmd, env=env)
        code = ret.returncode
        return
        
        if code != 0:
            print("return code:", code)
            print("failed cmd (cmd cannot contain empty str):", ' '.join(cmd))

        cmd = 'python refl_log_parser.py ' + bench
        print(cmd)
        ret = subprocess.run(cmd.split(), cwd='..')
        print("return code:", ret.returncode)
        outDir = '../poa-classes/'+bench

        if os.path.isdir(outDir):
            shutil.rmtree(outDir)

        os.makedirs(outDir)
        os.rename('out', outDir)
    

def compileOHAHelper(callback):
    if not callback:
        return
    cwd = os.getcwd()
    os.chdir(gg.getProjectDir())
    os.system('javac -cp ea-jars/plume-util-1.0.4.jar:ea-jars/new-ea.jar:harness harness/%s.java' % callback)
    os.chdir(cwd)
    print('compiled callback class, cwd:', cwd)


def buildBench(bench, cfg, opts="", logout=''):
    cwd = os.getcwd() + '/../'
    #classpath = '/home/tong/projects/soot/target/sootclasses-trunk-jar-with-dependencies.jar:%s/ea-classes:%s/ea-jars/new-ea.jar:%s/ea-jars/javabdd-1.0b2.jar' % (cwd, cwd, cwd)
    classpath = '%s/ea-classes:%s/ea-jars/new-ea.jar:%s/ea-jars/javabdd-1.0b2.jar' % (cwd, cwd, cwd)
    cmd = [gg.java, '-Xmx24g', '-ea', '-cp', classpath, 'oha.Main', '-bench', bench, '-out-class', cfg]
    if type(opts) == str:
        cmd += opts.replace('$B', bench).split()
    elif type(opts) == list:
        cmd += opts
    print("cmd:", ' '.join(cmd))
    
    start = time.time()
    ofs = None
    if logout:
        if logout == 1:
            ofs = open('ealog.out', 'w')
        else:
            assert type(logout) == str
            ofs = open(logout.replace('$B', bench), 'w')
    ret = subprocess.run(cmd, cwd=os.getcwd()+'/../', stdout=ofs)

        #ret = subprocess.run(cmd, cwd='/home/tong/projects/soot-projects/new-ea/target/')
    print("return code:", ret.returncode, ', total time:', time.time() - start)
    printCurrentTime()
    return ret.returncode


def printCurrentTime():
    print("Time stamp:", datetime.datetime.now())


def build(benches=None, cfgs=None, opts=None, logout=''):
    ret = compileNewEA()
    if ret != 0:
        return ret
    
    bms = gg.bms
    if type(bms) == str:
        bms = [bms]
    if cfgs == 'all':
        cfgs = ['vanilla', 'counter', 'counter+blockID',
               'counter+intraEA', 'counter+interEA',
               #'counter+interEA+OHA'
        ]

    for bench in getBenches(benches):
        for cfg in cfgs:
            ret = buildBench(bench, cfg, opts, logout)
            if ret != 0:
                print('bench build failed:', bench)
                return ret


def compileNewEA():
    cwd = os.getcwd()
    os.chdir('/home/tong/projects/soot-projects/new-ea')
    ret = os.system('./build.fish')
    os.chdir(cwd)
    return ret

def compileTests():
    cwd = os.getcwd()
    os.chdir('/home/tong/projects/soot-projects/new-ea')
    ret = os.system('./build-test.fish')
    os.chdir(cwd)
    
    if ret != 0:
        sys.exit(ret)
    #return ret

def buildAll():
    gg.setAllBenches()
    build()

    
def makeCnf(bench, ilevel):
    makeNewJar(bench, ilevel)

    
# def makeCnf(bench, ilevel=0):
#     print(os.getcwd())
#     orig_config = '../cnf-orig/%s.cnf' % bench
#     new_config = '../cnf/%s.cnf' % bench
#     ofs = open(new_config, 'w')
#     print("make cnf for ilevel", ilevel)

#     ohaJar = 'oha-%s-i%d.jar' % (bench, ilevel)
#     for line in open(orig_config):
#         if line.startswith('  jars'):
#             line = line.replace('  jars', '  jars "%s",' % ohaJar, 1)

#         ofs.write(line)
#     ofs.close()

def getJar(bench):
    if bench.startswith('trade'):
        return 'daytrader.jar'
    elif bench == 'avrora':
        return "avrora-cvs-20091224.jar"
    elif bench == 'h2':
        return "dacapo-h2.jar"
    elif bench == 'luindex':
        return "dacapo-luindex.jar"
    elif bench == 'lusearch':
        return "dacapo-lusearch.jar"
    elif bench == 'pmd':
        return "pmd-4.2.5.jar"
    elif bench == 'sunflow':
        return "sunflow-0.07.2.jar"
    elif bench == 'xalan':
        return "dacapo-xalan.jar"
    return bench+'.jar'


def makeNewJar(bench, cfg):
    cmd1 = 'cp ../orig-jar/*.jar ../jar/'
    print(cmd1)
    if os.system(cmd1) != 0:
        print("error: " + cmd1)

    if not cfg:
        return

    ohaJarDir = '../oha-jar/%s-%s/' % (bench, cfg)
    if not os.path.isdir(ohaJarDir):
        print('path not exist:', d)
        sys.exit(0)

    cmd2 = 'jar uf ../jar/%s -C %s .' % (getJar(bench), ohaJarDir)
    print(cmd2)
    if os.system(cmd2) != 0:
        print("error: " + cmd2)


def getOptStr(spark=False, refl=False):
    opts = ''
    if spark:
        opts += '-use-spark true '
    if refl:
        opts += '-use-refl true '
    return opts    


def buildAndRunDynamicEscape(bench, spark=True, refl=True, opts=''):
    opts += getOptStr(spark=spark, refl=refl)
    build([bench], ils=['checkThread'], opts=opts)
    run(bench, cfg='checkThread', n=10, callback='OHART_DynamicEscape')


def buildAndRunBlockCounts(bench, opts='', spark=True, refl=True, runbuild=True):
    opts += getOptStr(spark=spark, refl=refl)
    opts += ' -skip-static-init true -profile-mode'
    print(opts)
    ret = 0
    if runbuild:
        ret = build([bench], ils=['blockID'], opts=opts, logstdout=0)
    #if ret == 0:    
    run(bench, cfg='blockID', n=10, callback='OHART_BlockCounts')


def runEATests(benches=None, recompile=1, usebdd=0, bbinv=0, fromasm=1, classinv=0, finalinv=1,
               userefl=0, verifymode=1, skiplibs=1, logout=0):
    if recompile:
        compileTests()
        asm.runAsmPass('test-classes', istest=1, zipjar=0)
        
    
    if benches == None:
        benches = []
        #benches.append('TestPrograms.TestThread1')
        for i in range(1,17):
            if i == 12:
                continue
            benches.append('TestPrograms.Test%d' % i)

    runInterEA(benches, usebdd=usebdd, bbinv=bbinv, fromasm=fromasm,
               classinv=classinv, finalinv=finalinv, userefl=userefl,
               verifymode=verifymode, skiplibs=skiplibs, logout=logout,
               usespark=1)        
 

def buildTests(benches=None, usebdd=0, bbinv=0, fromasm=1, classinv=0, finalinv=1,
               userefl=0, verifymode=1, skiplibs=1, logout=0):
    if benches == None:
        benches = []
        #benches.append('TestPrograms.TestThread1')
        for i in range(1,17):
            if i == 12:
                continue
            benches.append('TestPrograms.Test%d' % i)
    
    # benches.append('TestPrograms.TestSort1') 
    # build(benches, cfgs=['counter+interEA'],
    #       opts='-skip-static-init true -round 3 -verify-mode true -use-bdd true -from-asm true -use-spark true')
    runInterEA(benches, usebdd=usebdd, bbinv=bbinv, fromasm=fromasm,
               classinv=classinv, finalinv=finalinv, userefl=userefl,
               verifymode=verifymode, skiplibs=skiplibs, logout=logout,
               usespark=0)

    
def makeBlockProfile(bench, start, end, skips=None):
    os.system('mkdir -p ../bms/' + bench)
    f = open('../bms/%s/blockCounts-i1.txt' % bench, 'w')
    for i in range(start, end+1):
        if i in skips:
            continue
        f.write('%d 1\n' % i)
    f.close()    
        

def runInterEA(benches=None, bbinv=0, finalinv=0, logout='',
               classinv=0, callgraph=0, userefl=0, usebdd=0,
               fromasm=0, usespark=1, skipjdk=1, verifymode=0,
               skipjavatime=0, bbprofile='', skipstaticinit=0,
               skiplibs=0, tracebddsrc=0,
               extraopts=''):
    
    opts = '-skip-lib-hashcode true -print-mapsto-graph false -cg-print-mapsto false'
    if usespark:
        opts += ' -use-spark true'
    else:
        opts += ' -use-spark false'
        
    if bbinv:
        if bbprofile:
            opts += ' -bb-profile "%s" -bb-inv true' % bbprofile
        else:
            opts += ' -bb-profile "bms/$B/blockCounts-i1.txt" -bb-inv true'
    else:
        if bbprofile:
            opts += ' -bb-profile "%s"' % bbprofile
    if finalinv:
        opts += ' -final-inv true'
    if classinv:
        opts += ' -class-profile "bms/$B/classes.txt"'
    if tracebddsrc:
        opts += ' -bdd-src-only true'
    if userefl:
        opts += ' -use-refl true'
    if callgraph:
        opts += ' -print-callgraph'
    if usebdd:
        opts += ' -use-bdd true'
    if fromasm:
        opts += ' -from-asm true'
    if verifymode:
        opts += ' -verify-mode true'
    if skiplibs:
        opts += ' -skip-lib-methods true'
    if skipjdk:
        opts += ' -jdk-as-lib true'
    if skipjavatime:
        opts += ' -skip-java-time true'
    if skipstaticinit:
        opts += ' -skip-static-init true'
    opts += ' ' + extraopts

    build(benches, cfgs=['counter+interEA'], opts=opts, logout=logout)
    #build(benches, ils=['counter'], opts=opts, logstdout=logstdout)
       
    

if __name__ == "__main__":
    gg.useEA = 1
    # gg.java = path_to_openj9_java
    runj9(ils=2, n=30)
