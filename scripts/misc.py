import sys
def getMissesSites(bench):
    staticSites = []
    dynamicSites = []
    for line in open('../bms/%s/escapedSites.txt' % bench):
        site = int(line.strip())
        dynamicSites.append(site)

    for line in open('../bms/%s/ea_logger.txt' % bench):
        if line.startswith('Escaped site:'):
            site = int(line.strip().split()[2])
            staticSites.append(site)

    #print(staticSites)
    for site in dynamicSites:
        if site not in staticSites:
            print("missing", site)


def getLoadedClasses(bench):
    classes = []
    for line in open('../bench.out'):
        if line.startswith('[Loaded '):
            cls = line.split()[1]
            classes.append(cls)

    ofs = open('../bms/%s/classes.txt' % bench, 'w')
    for cls in classes:
        ofs.write(cls+'\n')
    ofs.close()
    print('done writing to ' + '../bms/%s/classes.txt' % bench)
    
            
import re
def getHSRSSNumbers(bench):
    print(bench)
    for line in open(bench+"_hs.time"):
        if 'Maximum resident set size' in line:
            size = int(re.findall(r'\d+', line)[0])
            print(size//1024)
        

def getJ9RSSNumbers(bench):
    print(bench)
    for line in open(bench+"_j9.time"):
        if 'Maximum resident set size' in line:
            size = int(re.findall(r'\d+', line)[0])
            print(size//1024)

            
if __name__ == "__main__":
    benches = ['eclipse', 'lusearch', 'luindex', 'pmd', 'xalan']
    # for b in benches:
    #     getJ9RSSNumbers(b)

    bench = sys.argv[1]
    #getMissesSites(bench)

    getLoadedClasses(bench)
