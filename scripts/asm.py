import subprocess
import os
import sys
import shutil
import time
import datetime

import gg

def compilePass():
    cmd = '/usr/bin/fish build.sh'
    ret = subprocess.run(cmd.split(), cwd='/home/tong/projects/asm-projects/oha')
    return ret.returncode

def runAsmPass(benches, dumpclass=1, skiperror=1, dryrun=0, istest=0,
               zipjar=1):
    if compilePass() != 0:
        print("compile asm pass failed")
        return
    
    if type(benches) == str:
        benches = [benches]
        
    for bench in benches:
        cmd = ['java', '-ea']
        cmd = appendClasspath(cmd, bench)
        cmd += ['Main']
        cmd += ['-input-dir', bench]

        # -- Parse options
        optstrmap = {0: 'false', 1: 'true'}
        cmd += ['-dump-class', optstrmap[dumpclass]]
        cmd += ['-skip-transform-error', optstrmap[skiperror]]
        cmd += ['-skip-unfound-class', optstrmap[skiperror]]
        cmd += ['-dry-run', optstrmap[dryrun]]

        print(' '.join(cmd))

        # -- Run the ASM pass
        classdir = '../jar-classes/'
        testclassdir = '../'
        if istest:
            classdir = testclassdir
        ret = subprocess.run(cmd, cwd=classdir,
                             stdout=open("stdout.log", 'w'),
                             stderr=open("stdout.log", 'w'))
        rtcode = ret.returncode 
        if rtcode != 0:
            print('%s aborted, ret code: %d' % (bench, rtcode))
            os.system('cat stdout.log')
            return
        
        try:
            os.rename(classdir+'asmDump/', classdir+'/oha-'+bench)
        except OSError:
            os.system('rm -rf ' + classdir+'/oha-'+bench)
            os.rename(classdir+'asmDump/', classdir+'/oha-'+bench)

        if zipjar:
            zipJar(bench)
    

def zipJar(bench):
    for j in gg.benchjars[bench]:
        ret = os.system('cp ../orig-jar/%s ../jar/' % j)
    ret = os.system('jar uvf ../jar/%s -C ../jar-classes/oha-%s/ .' % (getBenchJar(bench), bench))
    if ret != 0:
        sys.exit(ret)

                
def getBenchJar(bench):
    return gg.benchjars[bench][0]
    # if bench == 'luindex':
    #     return "dacapo-luindex.jar"
    # elif bench == 'lusearch':
    #     return "dacapo-lusearch.jar"
    # else:
    #     raise Exception("Unknow jar for bench " + bench)


def appendClasspath(cmd, bench):
    assert type(cmd) == list
    return cmd + ['-cp', '/home/tong/projects/asm-projects/oha/target/MemOpInstrumenter.jar:%s' % bench]
    
