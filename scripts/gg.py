import platform
import os

hotspot = '/usr/lib/jvm/java-8-openjdk-amd64/bin/java'
#hotspot = '/home/tong/software/graalvm-ce-java8-20.1.0/bin/java'
#hotspot = '/home/tong/projects/dev-442a69af7bad/build/linux-x86_64-server-release/jdk/bin/java'
java = hotspot
# j9 = '/home/tong/projects/openj9-projects/openj9-openjdk8-oha//build/linux-x86_64-normal-server-release/images/j2re-image/bin/java'
# j9debug = '/home/tong/projects/openj9-projects/openj9-openjdk8-oha//build/linux-x86_64-normal-server-slowdebug/images/j2re-image/bin/java'

j9 = os.getenv("HOME") + '/openj9-local/jvm/openjdk-1.8.0_222-internal/bin/java'
#j9 = os.getenv("HOME") + '/software/jdk-11.0.7+10/bin/java'
bench = ''
bms = ['luindex']
#bms = ['pmd', 'fop', 'luindex', 'lusearch', 'xalan']
allBms = ['avrora', 'pmd', 'fop', 'luindex', 'lusearch', 'xalan', 'sunflow', 'h2']
eaJar = 'ea-jars/new-ea.jar'
poaJar = 'ea-jars/poa-2.0.3.jar'
fake = False
dryrun = False
useEA = True

benchjars = {
    'luindex': ["dacapo-luindex.jar", "lucene-core-2.4.jar", "lucene-demos-2.4.jar"],
    'avrora': ["avrora-cvs-20091224.jar"],
    'lusearch': ["dacapo-lusearch.jar", "lucene-core-2.4.jar", "lucene-demos-2.4.jar"],
    'sunflow': ["sunflow-0.07.2.jar", "janino-2.5.15.jar"],
}

if platform.system() == 'Darwin':
    java = 'java'


def sb(benches):
    global bms
    if isinstance(benches, str):
        benches = [benches]

    bms = benches    


def printBenches():
    print(bms)


def setAllBenches():
    global bms
    bms = allBms


def removeBench(bench):
    global bms
    newbms = []
    for b in bms:
        if bench in b:
            continue
        newbms.append(b)
    bms = newbms
    

def printAllBenches():
    print(allBms)


def printCmdOnly(v=True):
    global fake        
    fake = bool(v)


def getProjectDir():
    return '../'


def getBenchDir(b):
    return getProjectDir() + 'bms/' + b + '/'


def getBenchBDDNum(b):
    if b == 'avrora':
        return 40000
    elif b == 'luindex':
        return 20000
