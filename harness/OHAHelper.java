import org.dacapo.harness.Callback;
import org.dacapo.harness.CommandLineArgs;
import java.io.*;
import java.lang.reflect.Array;
import java.util.*;

import oha.Graph.WeightedConnGraph;

import static java.lang.System.in;
import static java.lang.System.out;

public class OHAHelper extends Callback {
  
  public static boolean handlerInstalled = false;
  public static long counter = 0;
  public static boolean dumpCounters = false;
  //public static Map<Integer, Integer> freqMap = new HashMap<>();
  public static boolean useEA = false;
  public static boolean blockCounts[] = new boolean[1<<20];
  public static int blockInv[] = new int[1<<20];
  public static boolean optimized = false;
  public static boolean shouldElide = true;
  public static boolean localElideMap[] = new boolean[800000];
  public static boolean slowPath[] = new boolean[100000];
    public static boolean slowPathFlag = true;
  
  public static Set<Integer> blockIDs = new HashSet<>();
  public static Map<Object, Integer> objMap = new HashMap<>();


  /* EA */
  static WeightedConnGraph cng = null;

  public static PrintWriter iterCountPrinter;
  public static int iter = 0;

  static {
    Arrays.fill(slowPath, true);
    Arrays.fill(blockInv, 1);
    installExitHandler();
  }

  public OHAHelper(CommandLineArgs args) {
    super(args);

    initIterCountPrinter();

    String flag = System.getenv("UseEA");
    if (flag != null) {
      useEA = true;
    }

    out.println("UseEA " + useEA);
    // if (useEA) {          
    //   initConnGraph();          
    // }      

  }

  void initConnGraph() {
    String fileName = System.getenv("OHA_cng");
    if (fileName == null) {
      out.println("No ConnGraph found");
      return;
    }

    cng = new WeightedConnGraph();
    cng.load(fileName);
  }

  void initIterCountPrinter() {
    String benchDir = System.getenv("OHA_benchDir");
    if (benchDir == null) {
      dumpCounters = false;
      return;
    }

    new File(benchDir).mkdirs();
    String fileName = benchDir+"iterCounts.txt";
    if (System.getenv("UseEA") != null) {
      fileName = benchDir+"bb-iterCounts.txt";
    }

    try {
      iterCountPrinter = new PrintWriter(fileName);
    } catch (Exception e) {
      e.printStackTrace();
      System.exit(1);
    }

    dumpCounters = true;
  }

  /* Immediately prior to start of the benchmark */
  @Override
  public void start(String benchmark) {
    System.err.println("oha hook starting " + (isWarmup() ? "warmup " : "") + benchmark);
    super.start(benchmark);
  };

  /* Immediately after the end of the benchmark */
  @Override
  public void stop(long duration) {
    super.stop(duration);
    // System.err.println("oha hook stopped " + (isWarmup() ? "warmup" : ""));
    // System.err.flush();
        
  };

  @Override
  public void complete(String benchmark, boolean valid) {
    super.complete(benchmark, valid);
    System.err.println("oha hook " + (valid ? "PASSED " : "FAILED ") + (isWarmup() ? "warmup " : "") + benchmark);
    System.err.println("Final counter: " + OHAHelper.counter);

    if (dumpCounters) {
      iterCountPrinter.println("Final counter: " + OHAHelper.counter);
      iterCountPrinter.flush();	    
    }

    OHAHelper.counter = 0;
    iter += 1;

    if (true) return;
    if (!useEA) {
      return;
    }

    int intraThresh = 20;
    int interThresh = 30;
    int interBBThresh = 40;
    //        String s = System.getenv("OHA_interThresh");
    //        if (s != null) {
    //          interThresh = Integer.parseInt(s);
    //        }
    //
    //        s = System.getenv("OHA_interBBThresh");
    //        if (s != null) {
    //          interBBThresh = Integer.parseInt(s);
    //        }

    if (iter == intraThresh) {
      runIntraEA();
    }
    else if (iter == interThresh) {
      //shouldElide = true;
      //Arrays.fill(localEscMap, true);
      runInterEA();
    }
    else if (iter == interBBThresh) {
      runInterEABB();
    }

  };

  public static void start() {
    System.out.println("<da>: OHAMemCount starts..");
  }

  public static void installExitHandler() {
    Runtime.getRuntime().addShutdownHook(new Thread()
      {
        public void run()  {
          dump();
        }
      });

    System.out.println("install exit handler");
  }


    /**
     * For block inv detection
     */ 
    public static void blockID(short s1, short s2) {
        int i = s1 * 32767 + s2;
        // shouldn't need to check, we can remove block() for blocks
        // that already run
        System.out.println("shorts: " + s1 + " " + s2);
        System.out.println("i " + i);
        System.out.println(blockCounts);
        if (blockCounts[i]) {
            return;
        }
        
        blockCounts[i] = true;
    }
    
    
  /**
   * For block inv detection
   */ 
  public static void blockID(int i) {
    // shouldn't need to check, we can remove block() for blocks
    // that already run
    if (blockCounts[i]) {
      return;
    }

    blockCounts[i] = true;
    //out.println("set blockCounts " + i);

    /* If block is an assumed invariant */
    synchronized(blockCounts) {
      if (blockInv[i] == 0) {  // means that the block is assumed not to run
          /* uncommenting this will cause benchmark validation error */
          //out.printf("inv violation detected at block %d\n", i);
          Arrays.fill(slowPath, true);
      }
    }
  }

  /**
   * For profiling
   */ 
  public static void blockIDSlow(int i) {	
    if (blockCounts[i]) {
      return;
    }

    // ? synchronization
    blockCounts[i] = true;
    // synchronized(blockCounts) {
    //     blockCounts[i] = true;
    //     Arrays.fill(slowPath, true);
    // }
  }

  static void runInterEA() {
    if (cng == null) {
      return;
    }

    Set<Integer> ranBlocks = new HashSet<>();
    for (int i = 0; i < blockCounts.length; ++i) {
      //if (blockCounts[i]) {
      ranBlocks.add(i);
      //}
    }
    out.println(ranBlocks.size() + ";" + blockCounts.length);
    cng.propagateEscapeState(ranBlocks);
    //        out.println("1");
    cng.propagateToLocals(ranBlocks);
    //        out.println("2");
    cng.setLocalElideArray(localElideMap);
    cng.report();
    out.println("Run EA...");
  }

  static void runIntraEA() {
    String benchDir = System.getenv("OHA_benchDir");
    new File(benchDir).mkdirs();
    String fileName = benchDir+"intra-escaped.set";
    Set<Integer> escaped = loadIntSet(fileName);
    for (int i = 0; i < localElideMap.length; ++i) {
      if (!escaped.contains(i)) {
        localElideMap[i] = true;
      }
    }
  }

  static Set<Integer> loadIntSet(String fileName) {
    Set<Integer> map = null;
    try {
      FileInputStream fis = new FileInputStream(fileName);
      ObjectInputStream ois = new ObjectInputStream(fis);
      map = (HashSet) ois.readObject();
      ois.close();
      fis.close();
    } catch(IOException ioe) {
      ioe.printStackTrace();
    } catch(ClassNotFoundException c)
    {
      System.out.println("Class not found");
      c.printStackTrace();
    }
    return map;
  }

  static void runInterEABB() {
    if (cng == null) {
      return;
    }

    Set<Integer> ranBlocks = new HashSet<>();
    for (int i = 0; i < blockCounts.length; ++i) {
      if (blockCounts[i]) {
        ranBlocks.add(i);
      }
    }
    out.println(ranBlocks.size() + ";" + blockCounts.length);
    cng.resetEscapeInfo();
    cng.propagateEscapeState(ranBlocks);
    //        out.println("1");
    cng.propagateToLocals(ranBlocks);
    //        out.println("2");
    cng.setLocalElideArray(localElideMap);
    cng.report();
    out.println("Run EA...");
  }

  static void dumpMap(Map<Integer, Integer> map, String fileName) {
    PrintWriter printWriter = null;
    try {
      //FileWriter fileWriter = new FileWriter(fileName);
      printWriter = new PrintWriter(fileName);
      for (Integer i: map.keySet()) {
        printWriter.printf("%d %d\n", i, map.get(i));
      }
    }
    catch (Exception e) {
      e.printStackTrace();
      System.exit(1);
    }
    finally {
      // always close the output stream
      if(printWriter != null){
        printWriter.close();
      }
    }
  }

  static void dumpArray(boolean[] arr, String fileName) {
    PrintWriter printWriter = null;
    try {
      //FileWriter fileWriter = new FileWriter(fileName);
      printWriter = new PrintWriter(fileName);
      for (int i = 0; i < arr.length; ++i) {
        if (arr[i]) {
          printWriter.printf("%d %d\n", i, 1);
        }
      }
    }
    catch (Exception e) {
      e.printStackTrace();
      System.exit(1);
    }
    finally {
      // always close the output stream
      if(printWriter != null){
        printWriter.close();
      }
    }
  }

  static void dumpArray(int[] arr, String fileName) {
    PrintWriter printWriter = null;
    try {
      //FileWriter fileWriter = new FileWriter(fileName);
      printWriter = new PrintWriter(fileName);
      for (int i = 0; i < arr.length; ++i) {
        if (arr[i] > 0) {
          printWriter.printf("%d %d\n", i, arr[i]);
        }
      }
    }
    catch (Exception e) {
      e.printStackTrace();
      System.exit(1);
    }
    finally {
      // always close the output stream
      if(printWriter != null){
        printWriter.close();
      }
    }
  }


  static void dump() {
    out.println("Final counter: " + counter);
    //dumpArray(blockCounts, "blockCounts.txt");
  }

  public static void inc() {	
    counter++;
    //out.println("counter: " + counter);
  }


  public static void inc(int id, java.lang.Object o) {
    inc();
  }

  public static void inc(int id) {
    inc();
  }

  public static void dumpCounter() {
    System.out.println("final count: " + counter);
  }

  public static void incField(int id, java.lang.Object o) {
    inc();
  }

  public static void incArray(int id, java.lang.Object o) {
    inc();
  }

  public static void incStatic(int id) {
    inc();
  }

  public static void incByBase(int id) {
    // if (localElideMap[id]) {
    //     return;
    // }           
      inc();
  }
    
  public static void incByBaseSlow(int id) {
    inc();
  }
}

