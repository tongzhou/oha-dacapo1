import java.io.*;
import java.lang.reflect.Array;
import java.util.*;
import java.util.concurrent.atomic.*;


import static java.lang.System.in;
import static java.lang.System.out;

public class OHABaseRT {  
    public static boolean handlerInstalled = false;
    public static long counter = 0;
    public static AtomicLong atomicCounter = new AtomicLong();
    public static int iter = 0;
    public static int phase = 0;
    public static boolean dumpCounters = false;

    public static boolean blockCounts[] = new boolean[1<<20];
    public static AtomicIntegerArray atomicblockCounts = new AtomicIntegerArray(1<<20);
    public static int blockInv[] = new int[1<<20];
    public static boolean optimized = false;
    public static boolean shouldElide = true;
    public static boolean localElideMap[] = new boolean[800000];
    public static boolean slowPath[] = new boolean[100000];
    public static boolean slowPathFlag = false;
  

    static {
        Arrays.fill(slowPath, true);
        Arrays.fill(blockInv, 1);
        installExitHandler();
    }

    public static void installExitHandler() {
        Runtime.getRuntime().addShutdownHook(new Thread()
            {
                public void run()  {
                    dump();
                }
            });

        //System.out.println("install exit handler");
    }


    /**
     * For block inv detection
     */ 
    public static void blockID(short s1, short s2) {
        int i = s1 * 32767 + s2;
        atomicblockCounts.getAndIncrement(i);

        /*
        synchronized(OHABaseRT.class) {
            if (atomicCounter.get() > 40000000) {            
            
                String fn = "bms/sunflow/blockCounts-epoch" + phase + ".txt";
                dumpAtomicArray(atomicblockCounts, fn);
                for (int j = 0; j < atomicblockCounts.length(); j++) {
                    atomicblockCounts.set(j, 0);
                }
                phase++;
                atomicCounter.set(0);
            }
        }
        */

        
        if (i == 517) {
            synchronized(OHABaseRT.class) {
                System.out.println("===== OHABaseRT stack trace ======");
                System.out.println("block id: " +i);
                for (StackTraceElement ste : Thread.currentThread().getStackTrace()) {
                    System.out.println(ste);
                }
            }
        }
        
        
        /*
        //out.printf("s1: %d, s2: %d, i: %d\n", s1, s2, i);
        // shouldn't need to check, we can remove block() for blocks
        // that already run
        if (blockCounts[i]) {
            return;
        }
        
        blockCounts[i] = true;
        */
        
    }
    
    
    /**
     * For block inv detection
     */ 
    public static void blockID(int i) {
        // shouldn't need to check, we can remove block() for blocks
        // that already run
        if (blockCounts[i]) {
            return;
        }

        blockCounts[i] = true;
        //out.println("set blockCounts " + i);

        /* If block is an assumed invariant */
        synchronized(blockCounts) {
            if (blockInv[i] == 0) {  // means that the block is assumed not to run
                /* uncommenting this will cause benchmark validation error */
                //out.printf("inv violation detected at block %d\n", i);
                Arrays.fill(slowPath, true);
            }
        }
    }

    /**
     * For profiling
     */ 
    public static void blockIDSlow(int i) {	
        if (blockCounts[i]) {
            return;
        }

        // ? synchronization
        blockCounts[i] = true;
        // synchronized(blockCounts) {
        //     blockCounts[i] = true;
        //     Arrays.fill(slowPath, true);
        // }
    }


    static void dumpMap(Map<Integer, Integer> map, String fileName) {
        PrintWriter printWriter = null;
        try {
            //FileWriter fileWriter = new FileWriter(fileName);
            printWriter = new PrintWriter(fileName);
            for (Integer i: map.keySet()) {
                printWriter.printf("%d %d\n", i, map.get(i));
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
        finally {
            // always close the output stream
            if(printWriter != null){
                printWriter.close();
            }
        }
    }

    static void dumpArray(boolean[] arr, String fileName) {
        PrintWriter printWriter = null;
        try {
            //FileWriter fileWriter = new FileWriter(fileName);
            printWriter = new PrintWriter(fileName);
            for (int i = 0; i < arr.length; ++i) {
                if (arr[i]) {
                    printWriter.printf("%d %d\n", i, 1);
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
        finally {
            // always close the output stream
            if(printWriter != null){
                printWriter.close();
            }
        }
    }

    static void dumpArray(int[] arr, String fileName) {
        PrintWriter printWriter = null;
        try {
            //FileWriter fileWriter = new FileWriter(fileName);
            printWriter = new PrintWriter(fileName);
            for (int i = 0; i < arr.length; ++i) {
                if (arr[i] > 0) {
                    printWriter.printf("%d %d\n", i, arr[i]);
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
        finally {
            // always close the output stream
            if(printWriter != null){
                printWriter.close();
            }
        }
    }


    static void dumpAtomicArray(AtomicIntegerArray arr, String fileName) {
        PrintWriter printWriter = null;
        try {
            //FileWriter fileWriter = new FileWriter(fileName);
            printWriter = new PrintWriter(fileName);
            for (int i = 0; i < arr.length(); ++i) {
                int v = arr.get(i);
                if (v > 0) {
                    printWriter.printf("%d %d\n", i, v);
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
        finally {
            // always close the output stream
            if(printWriter != null){
                printWriter.close();
            }
        }
    }


    static void dump() {
        out.println("Final counter: " + atomicCounter.get());
        //dumpArray(blockCounts, "blockCounts.txt");
        //dumpArray(blockRealCounts, "blockCounts.txt");
        dumpAtomicArray(atomicblockCounts, "blockCounts.txt");
    }

    public static void inc() {	
        //counter++;
        atomicCounter.getAndIncrement();
    }


    public static void inc(int id, java.lang.Object o) {
        inc();
    }

    public static void inc(int id) {
        inc();
    }

    public static void inc(short id) {
        inc();
    }

    public static void inc(short s1, short s2) {
        inc();
    }

    public static void dumpCounter() {
        System.out.println("final count: " + counter);
    }

    public static void incField(int id, java.lang.Object o) {
        inc();
    }

    public static void incArray(int id, java.lang.Object o) {
        inc();
    }

    public static void incStatic(int id) {
        inc();
    }

    public static void incByBase(int id) {
        // if (localElideMap[id]) {
        //     return;
        // }           
        inc();
    }
    
    public static void incByBaseSlow(int id) {
        inc();
    }
}

