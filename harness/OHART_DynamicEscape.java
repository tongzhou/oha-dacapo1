/*
 * Copyright (c) 2006, 2009 The Australian National University.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Apache License v2.0.
 * You may obtain the license at
 * 
 *    http://www.opensource.org/licenses/apache2.0.php
 */
import org.dacapo.harness.Callback;
import org.dacapo.harness.CommandLineArgs;

import java.lang.Object;
import java.util.*;
import java.io.*;
import org.plumelib.util.WeakIdentityHashMap;

/**
 * This runtime implements a dynamic analysis that tracks escaping
 * sites, and escape objects.
 *
 * The key point is to se a WeakIdentityHashMap. Without weak reference,
 * none of the objects can be claimed. Memory runs out quickly.
 * Without the identify hashmap, the map will call into application's
 * hashCode and equals, which we don't want.
 */
public class OHART_DynamicEscape extends Callback {
  static IdentityHashMap<Object, List<Long>> objAccessThreads = new IdentityHashMap<>();
  //static IdentityHashMap<Object, Long> objThread = new IdentityHashMap<>();
  static WeakIdentityHashMap<Object, Long> objThread = new WeakIdentityHashMap<>();
  static Set<Integer> escapedSites = new HashSet<>();
  static IdentityHashMap<Object, Long> escapedObjs = new IdentityHashMap<>();
  //static IdentityHashMap<Integer, Integer> siteCounts = new IdentityHashMap<>();
  static long[] siteCounts = new long[100000];
  static long accessCount = 0;
  static boolean reported = false;

  public OHART_DynamicEscape(CommandLineArgs args) {
    super(args);
    installExitHandler();
  }

  public static void installExitHandler() {
    Runtime.getRuntime().addShutdownHook(new Thread()
      {
        public synchronized void run()  {
          //report();
          if (!reported) {
            reportEscapedSites();
          }
          reported = true;
        }
      });

    //System.out.println("install exit handler");
  }

  /* Immediately prior to start of the benchmark */
  @Override
  public void start(String benchmark) {
    System.err.println("my hook starting " + (isWarmup() ? "warmup " : "") + benchmark);
    super.start(benchmark);
  };

  /* Immediately after the end of the benchmark */
  @Override
  public void stop(long duration) {
    super.stop(duration);
    System.err.println("stop");
    System.err.println("my hook stopped " + (isWarmup() ? "warmup" : ""));
    System.err.flush();
    
  };

  @Override
  public void complete(String benchmark, boolean valid) {
    super.complete(benchmark, valid);
    System.err.println("complete");
    System.err.println("my hook " + (valid ? "PASSED " : "FAILED ") + (isWarmup() ? "warmup " : "") + benchmark);
    System.err.flush();

    //report
  };

  static public void checkThread(int i, java.lang.Object o) {
    //if (true) return;
    Thread currentThread = Thread.currentThread();
    long tid = currentThread.getId();

    synchronized(OHART1.class) {
      siteCounts[i]++;
      accessCount++;
      
      
      if (!objThread.containsKey(o)) {
        objThread.put(o, tid);
      }
      
      long objTid = objThread.get(o);
      if (objTid != tid) {
        escapedSites.add(i);
        escapedObjs.put(o, (long)1);

        // if (i == 6173) {
        //     System.out.println("========== Call Stack =========");
        //     for (StackTraceElement ste : Thread.currentThread().getStackTrace()) {
        //         System.out.println(ste);
        //     }            
        // }
      }
      
      if (escapedObjs.containsKey(o)) {
        escapedObjs.put(o, escapedObjs.get(o) + 1);
      }

      
      
    }

    //int hash = System.identityHashCode(o);
    
  }

  static public void recordAccessThread(java.lang.Object o) {
    //if (true) return;
    Thread currentThread = Thread.currentThread();
    long tid = currentThread.getId();

    synchronized(OHART1.class) {
      if (!objAccessThreads.containsKey(o)) {
        objAccessThreads.put(o, new ArrayList<>());
      }
      List<Long> threads = objAccessThreads.get(o);
    
      if (!threads.contains(tid)) {
        threads.add(tid);
      }

    }

    //int hash = System.identityHashCode(o);
    
  }

  static public void report() {
    System.out.println("report");
    for (Object o: objAccessThreads.keySet()) {
      List<Long> threads = objAccessThreads.get(o);
      if (threads.size() > 1) {
        System.out.println(o);
      }
    }
  }

  static void reportEscapedSites() {
    synchronized(OHART1.class) {
      System.out.println("Escaped sites:");
      
      // for (int i: escapedSites) {
      //   System.out.println(i);
      // }

      List<Integer> sortedSites = new ArrayList<Integer>(escapedSites); 

      //Sort the list
      Collections.sort(sortedSites);
      String dir = System.getenv("OHA_benchDir");
      try (PrintWriter pw = new PrintWriter(dir+"escapedSites.txt")) {
        for (int i: sortedSites) {
          pw.println(i);
        }
      }
      catch (Exception e) {
        
      }


      System.out.println("Escaped objs: " + escapedSites.size());

      long totalCount = 0;
      long escapedSitesAccessCount = 0;
      long escapedObjsAccessCount = 0;
      for (int i = 0; i < siteCounts.length; ++i) {
        long thisCount = siteCounts[i];
        totalCount += thisCount;
        if (escapedSites.contains(i)) {
          escapedSitesAccessCount += thisCount;
        }
      }


      for (Object o: escapedObjs.keySet()) {
        escapedObjsAccessCount += escapedObjs.get(o);
      }
      //assert(count == accessCount);

      System.out.printf("%d, %d, %d, %d, %.3f, %.3f", accessCount, totalCount,
                        escapedSitesAccessCount, escapedObjsAccessCount,
                        ((float)escapedSitesAccessCount)/accessCount,
                        ((float)escapedObjsAccessCount)/accessCount
                        );
    }
  }
}
