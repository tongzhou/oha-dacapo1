/*
 * Copyright (c) 2006, 2009 The Australian National University.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Apache License v2.0.
 * You may obtain the license at
 * 
 *    http://www.opensource.org/licenses/apache2.0.php
 */
import org.dacapo.harness.Callback;
import org.dacapo.harness.CommandLineArgs;

import java.lang.Object;
import java.util.*;
import java.io.*;
import org.plumelib.util.WeakIdentityHashMap;
import java.util.concurrent.atomic.AtomicIntegerArray;
import static java.lang.System.out;

/**
 * This runtime profiles the counts of each block and dump to
 * "blockCounts.txt".
 */
public class OHART_BlockCounts extends Callback {   
    public static long blockCounts[] = new long[1<<20];
    public static AtomicIntegerArray atomicblockCounts = new AtomicIntegerArray(1<<20);
    static boolean reported = false;
    static boolean verbose = true;

    static {
        installExitHandler();
    }
    
    public OHART_BlockCounts(CommandLineArgs args) {
        super(args);
    }
    
    public static void installExitHandler() {
        Runtime.getRuntime().addShutdownHook(new Thread()
            {
                public synchronized void run()  {
                    //report();
                    if (!reported) {
                        report();
                    }
                    reported = true;
                }
            });
        
        //System.out.println("install exit handler");
    }

    /* Immediately prior to start of the benchmark */
    @Override
    public void start(String benchmark) {
        System.err.println("my hook starting " + (isWarmup() ? "warmup " : "") + benchmark);
        super.start(benchmark);
    };

    /* Immediately after the end of the benchmark */
    @Override
    public void stop(long duration) {
        super.stop(duration);
        System.err.println("stop");
        System.err.println("my hook stopped " + (isWarmup() ? "warmup" : ""));
        System.err.flush();
    
    };

    @Override
    public void complete(String benchmark, boolean valid) {
        super.complete(benchmark, valid);
        System.err.println("complete");
        System.err.println("my hook " + (valid ? "PASSED " : "FAILED ") + (isWarmup() ? "warmup " : "") + benchmark);
        System.err.flush();

        //report
    };

    // public static void blockID(int i) {
    //     synchronized(blockCounts) {
    //         blockCounts[i]++;
    //     }
    // }

    public static void blockID(int i) {
        atomicblockCounts.getAndIncrement(i);
    }

    static public void report() {        
        if (verbose) {
            System.out.println("report");
        }

        //dumpArray(blockCounts, "blockCounts.txt");
        dumpAtomicArray("blockCounts.txt");
    }

    static void dumpArray(long[] arr, String fileName) {
        
        System.out.println("to dump array to " + fileName);
        PrintWriter printWriter = null;
        try {
            //FileWriter fileWriter = new FileWriter(fileName);
            printWriter = new PrintWriter(fileName);
            for (int i = 0; i < arr.length; ++i) {
                if (arr[i] > 0) {
                    printWriter.printf("%d %d\n", i, arr[i]);
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
        finally {
            // always close the output stream
            if(printWriter != null){
                printWriter.close();
            }
        }
    }

    static void dumpAtomicArray(String fileName) {        
        System.out.println("to dump array to " + fileName);
        PrintWriter printWriter = null;
        try {
            //FileWriter fileWriter = new FileWriter(fileName);
            printWriter = new PrintWriter(fileName);
            for (int i = 0; i < atomicblockCounts.length(); ++i) {
                if (atomicblockCounts.get(i) > 0) {
                    printWriter.printf("%d %d\n", i, atomicblockCounts.get(i));
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
        finally {
            // always close the output stream
            if(printWriter != null){
                printWriter.close();
            }
        }
    }


}
